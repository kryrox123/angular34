import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { Usuario_Routes } from './components/prices/prices-routes';
import { PricesComponent } from './components/prices/prices.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'prices/:id', 
  component: PricesComponent,
  children: Usuario_Routes,
  },
  { path: 'about', component: AboutComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
